public class Main {
    public static void main(String[] args) {
        //семья 1
        Human mother = new Human("Victoria", "Karleone", 1965);
        Human father = new Human("Vito", "Karleone", 1970);
        Pet pet = new Pet("собака", " Rock", 5, 75,
                new String[]{"їсти", "пити", "спати"});

        Human child = new Human("Michael", "Karleone Jr.",
                1995, 90, pet, mother, father,
                new String[][]{{"Понеділок", "робота"}, {"Вівторок", "відпочинок"}});

        child.setPet(pet);

        Family family = new Family(mother, father, pet);
        family.addChild(child);
        family.setPet(pet);

        System.out.println("Інформація про дитину:");
        System.out.println(child.toString());

        System.out.println("Інформація про сім'ю:");
        System.out.println(family.toString());

        //семья 2
        Human mother1 = new Human("Мария", "Иванова", 1980);
        Human father1 = new Human("Иван", "Иванов", 1982);
        Pet cat1 = new Pet("кот", "Мурзик", 2, 45,
                new String[]{"спать", "кушать", "играть с клубком"});
        Human child1 = new Human("Анна", "Иванова",
                2005, 85, cat1, mother1, father1,
                new String[][]{{"Понедельник", "школа"}, {"Вторник", "спорт"}});

        Human child2 = new Human("Петр", "Иванов",
                2010, 75, cat1, mother1, father1,
                new String[][]{{"Понедельник", "школа"}, {"Вторник", "спорт"}});

        Family family1 = new Family(mother1, father1, cat1);
        family1.addChild(child1);
        family1.addChild(child2);

        //семья 3
        Human mother2 = new Human("Ольга", "Петрова", 1990);
        Human father2 = new Human("Алексей", "Петров", 1988);
        Pet parrot = new Pet("попугай", "Кеша", 1, 70,
                new String[]{"разговаривать", "прыгать по веткам"});
        Human child3 = new Human("Ирина", "Петрова",
                2015, 95, parrot, mother2, father2,
                new String[][]{{"Понедельник", "садик"}, {"Вторник", "кружок"}});

        Family family2 = new Family(mother2, father2, parrot);
        family2.addChild(child3);

        //выводим информацию о семьях
        System.out.println("Информация о семье 1:");
        System.out.println(family1.toString());

        System.out.println("Інформація про дитину:");
        System.out.println(child1.toString());

        System.out.println("Інформація про дитину:");
        System.out.println(child2.toString());


        System.out.println("\nИнформация о семье 2:");
        System.out.println(family2.toString());

        System.out.println("Інформація про дитину:");
        System.out.println(child3.toString());
    }
}
