public class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Pet pet;
    private Human mother;
    private Human father;
    private String[][] schedule;
    private Family family;

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, Human mother, Human father) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.mother = mother;
        this.father = father;
    }

    public Human(String name,
                 String surname,
                 int year, int iq,
                 Pet pet, Human mother,
                 Human father, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.pet = pet;
        this.mother = mother;
        this.father = father;
        this.schedule = schedule;
    }

    public static void main(String[] args) {
        Pet pet = new Pet("собака", " Rock", 5, 75,
                new String[]{"їсти", "пити", "спати"});

        Human mother = new Human("Jane", "Karleone", 1970);
        Human father = new Human("Vito", "Karleone", 1965);
        Human child = new Human("Michael", "Karleone Jr.",
                1995, 90, pet, mother, father,
                new String[][]{{"Понеділок", "робота"}, {"Вівторок", "відпочинок"}});

        System.out.println("Демонстрація методов greetPet та describePet:");
        child.greetPet();
        child.describePet();
    }

    public void greetPet() {
        System.out.println("Привіт, " + pet.getNickname() + "!");
    }

    public void describePet() {
        String intelligence = (pet.getTrickLevel() > 50) ? "дуже хитрий" : "не дуже хитрий.";
        System.out.println("В мене є " + pet.getSpecies() + ", їй " + pet.getAge() + " років, він " + intelligence);
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getYear() {
        return year;
    }

    public int getIq() {
        return iq;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public Human getMother() {
        return mother;
    }

    public Human getFather() {
        return father;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    @Override
    public String toString() {
        return "Human{name='" + name + "'," +
                " surname='" + surname + "'," +
                " year=" + year + ", iq=" + iq + "," +
                " mother=" + mother.getName() + " " + mother.getSurname() + "," +
                " father=" + father.getName() + " " + father.getSurname() + "," +
                " pet=" + pet.toString() + "}";
    }
}
