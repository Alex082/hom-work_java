import java.util.Objects;

public class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private String[][] schedule;
    private Family family;

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, Human mother, Human father) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name,
                 String surname,
                 int year, int iq,
                 Pet pet, Human mother,
                 Human father, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;

        this.family = new Family(mother, father, pet);
        mother.setFamily(this.family);
        father.setFamily(this.family);
    }

    public static void main(String[] args) {
        Pet pet = new Pet(Species.DOG, " Rock", 5, 75,
                new String[]{"їсти", "пити", "спати"});

        Human mother = new Human("Jane", "Karleone", 1970);
        Human father = new Human("Vito", "Karleone", 1965);
        Human child = new Human("Michael", "Karleone Jr.",
                1995, 90, pet, mother, father,
                new String[][]{{"Понеділок", "робота"}, {"Вівторок", "відпочинок"}});

        System.out.println("Демонстрація методов greetPet та describePet:");
        child.greetPet();
        child.describePet();
    }

    public void greetPet() {
        System.out.println("Привіт," + this.family.getPet().getNickname() + "!");
    }

    public void describePet() {
        String intelligence = (this.family.getPet().getTrickLevel() > 50) ?
                "дуже хитрий" : "не дуже хитрий.";
        System.out.println("В мене є " + this.family.getPet().getSpecies() +
                ", їй " + this.family.getPet().getAge() + " років, він " + intelligence);
    }


    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getYear() {
        return year;
    }

    public int getIq() {
        return iq;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return year == human.year &&
                iq == human.iq &&
                Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, year, iq);
    }


    @Override
    public String toString() {
        String motherName = (family.getMother() != null) ?
                family.getMother().getName() + " " + family.getMother().getSurname()
                : "немає інформації про матір";
        String fatherName = (family.getFather() != null) ?
                family.getFather().getName() + " " + family.getFather().getSurname()
                : "немає інформації про батька";
        String petInfo = (family.getPet() != null) ? family.getPet().toString()
                : "немає домашнього улюбленця";

        return "Human{name='" + name + "'," +
                " surname='" + surname + "'," +
                " year=" + year + ", iq=" + iq + "," +
                " mother=" + motherName + "," +
                " father=" + fatherName + "," +
                " pet=" + petInfo + "}";
    }
}
