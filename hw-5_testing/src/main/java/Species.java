public enum Species {
    DOG,
    CAT,
    PARROT,
    HAMSTER,
    UNKNOWN,
}
