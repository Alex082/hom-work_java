import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class FamilyTest {
    private Family family;
    private Human child;
    private Human mother;
    private Human father;
    private Pet pet;

    @Before
    public void setUp() {
        Human mother = new Human("Victoria", "Karleone", 1965);
        this.mother = mother;
        Human father = new Human("Vito", "Karleone", 1970);
        this.father = father;
        Pet pet = new Pet(Species.DOG, "Rock", 5, 75,
                new String[]{"їсти", "пити", "спати"});

        this.pet = pet;

        child = new Human("Michael", "Karleone Jr.", 1995, 90,
                pet, mother, father, new String[][]{{DaysWeek.MONDAY.name(), "робота"},
                {DaysWeek.TUESDAY.name(), "відпочинок"}});

        family = new Family(mother, father, pet);
        family.addChild(child);
        family.setPet(pet);
    }

    @Test
    public void testToString() {
        Family family = new Family(mother, father, pet);
        Human child = new Human("Michael", "Karleone Jr.", 1995);
        family.addChild(child);

        String expected = "Family{mother=Victoria Karleone, father=Vito Karleone," +
                " children=Michael Karleone Jr., pet=DOG{nickname='Rock'}}";
        String actual = family.toString();

        assertEquals(expected, actual);
    }

    @Test
    public void testDeleteChild(){
        Family family = new Family(new Human("Mother", "Surname", 1970),
                new Human("Father", "Surname", 1970),
                new Pet(Species.DOG, "Rock", 5, 75, new String[]{}));

        if (family.getChildren().length > 0) {
            int childIndex = 0;
            assertTrue(family.deleteChild(childIndex));
        }

        //int childIndex = family.indexof(child);
        //assertTrue(family.deleteChild(childIndex));
        //assertEquals(2, family.countFamily());
    }

    @Test
    public void testDeleteChildByIndex(){
        Family family = new Family(new Human("Mother", "Surname", 1970),
                new Human("Father", "Surname", 1970),
                new Pet(Species.DOG, "Rock", 5, 75, new String[]{}));

        if (family.getChildren().length > 0) {
            assertTrue(family.deleteChild(0));
        }

//        assertTrue(family.deleteChild(0));
//        assertEquals(2, family.countFamily());
    }

    @Test
    public void testAddChild(){
        Family family = new Family(new Human("Mother", "Surname", 1965),
                new Human("Father", "Surname", 1970),
                new Pet(Species.DOG, "Rock", 5, 75, new String[]{}));
        Human newChild = new Human("Test", "Child", 2000);
        family.addChild(newChild);
        assertEquals(3, family.countFamily());
    }

    @Test
    public void testAddChildNegative(){
        Family family = new Family(new Human("Mother", "Surname", 1970),
                new Human("Father", "Surname", 1970),
                new Pet(Species.DOG, "Rock", 5, 75, new String[]{}));

        assertFalse(family.deleteChild(-1));
        assertFalse(family.deleteChild(0));
        assertFalse(family.deleteChild(1));
    }

    @Test
    public void testCountFamily(){
        assertEquals(3, family.countFamily());
    }
}
