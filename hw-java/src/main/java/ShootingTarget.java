import java.util.Random;
import java.util.Scanner;

//package hw-2;
public class ShootingTarget {
    public static void main(String[] args) {
        int[][] gameBoard = new int[5][5];
        int targetRow, targetCol;
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);

        System.out.println("All Set. Get ready to rumble!");

        targetRow = random.nextInt(5);
        targetCol = random.nextInt(5);

        while (true) {
            System.out.print("Введите строку для стрельбы: (1-5)");
            int row = scanner.nextInt();
            System.out.print("Введите столбец для стрельбы: (1-5)");
            int col = scanner.nextInt();

            if (row < 1 || row > 5 || col < 1 || col > 5) {
                System.out.println("Пожалуйста, введите корректные значения для строки и столбца: (1-5)");
                continue;
            }

            row--;
            col--;

            if (row == targetRow && col == targetCol) {
                gameBoard[row][col] = 1;
                printGameBoard(gameBoard);
                System.out.print("Вы выиграли.");
                break;
            } else {
                gameBoard[row][col] = -1;
                printGameBoard(gameBoard);
            }
        }
        scanner.close();
    }

    public static void printGameBoard(int[][] board) {
        System.out.println("0 |   1 | 2 | 3 | 4 | 5 |");

        for (int i = 0; i < 5; i++) {
            System.out.print(i + 1 + " | ");

            for (int a = 0; a < 5; a++) {
                if (board[i][a] == 0) {
                    System.out.print("- | ");
                } else if (board[i][a] == -1) {
                    System.out.print("* | ");
                } else if (board[i][a] == 1) {
                    System.out.print("x | ");
                }
            }
            System.out.println();
        }
    }
}
