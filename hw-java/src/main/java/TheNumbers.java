import java.util.Random;
import java.util.Scanner;

public class TheNumbers {
    public static void main(String[] args) {
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);

        System.out.println("Let the game begin!");

        int secretNumber = random.nextInt(101);

        System.out.print("Enter your name:");

        String name = scanner.nextLine();

        while (true) {
            System.out.print("Guess the number:");
            int guess = scanner.nextInt();

            if(guess < secretNumber) {
                System.out.println("Your number is too small. Please, try again.");
            } else if (guess > secretNumber) {
                System.out.println("Your number is too big. Please, try again.");
            } else {
                System.out.println("Congratulations," + name + "!");
                break;
            }
        }
        scanner.close();
    }
}
