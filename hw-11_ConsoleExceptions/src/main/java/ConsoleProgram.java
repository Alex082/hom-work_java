import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ConsoleProgram {
    private final FamilyController familyController;
    private final Scanner scanner;
    private static List<Family> families = new ArrayList<>();
    public ConsoleProgram(FamilyController familyController, Scanner scanner) {
        this.familyController = familyController;
        this.scanner = scanner;
    }

    public static void main(String[] args) {
        FamilyDao familyDao = new FamilyDao() {
            @Override
            public List<Family> getAllFamilies() {
                return families;
            }

            @Override
            public Family getFamilyByIndex(int index) {
                if (index >= 0 && index < families.size()){
                    return families.get(index);
                }
                return null;
            }

            @Override
            public boolean deleteFamily(int index) {
                if (index >= 0 && index < families.size()) {
                    families.remove(index);
                    return true;
                }
                return false;
            }

            @Override
            public boolean deleteFamily(Family family) {
                return families.remove(family);
            }

            @Override
            public void saveFamily(Family family) {
                families.add(family);
            }
        };

        FamilyService familyService = new FamilyService(familyDao);
        FamilyController familyController = new FamilyController(familyService);
        Scanner scanner = new Scanner(System.in);

        ConsoleProgram consoleProgram = new ConsoleProgram(familyController, scanner);
        consoleProgram.start();
    }

    public void start() {
        while (true) {
            displayManu();
            int choice = getUserChoice();
            executeChoice(choice);
        }
    }

    private void displayManu() {
        System.out.println("Доступні команди:");
        System.out.println("1. Заповнити тестовими даними");
        System.out.println("2. Відобразити весь список сімей");
        System.out.println("3. Відобразити список сімей, де кількість людей більша за задану");
        System.out.println("4. Відобразити список сімей, де кількість людей менша за задану");
        System.out.println("5. Підрахувати кількість сімей, де кількість членів дорівнює");
        System.out.println("6. Створити нову родину");
        System.out.println("7. Видалити сім'ю за індексом сім'ї у загальному списку");
        System.out.println("8. Редагувати сім'ю за індексом сім'ї у загальному списку");
        System.out.println("9. Видалити всіх дітей старше віку");
        System.out.println("10. Вийти (exit)");
        System.out.print("Введіть номер команди: ");
    }

    private int getUserChoice() {
        while (true) {
            try {
                int choice = Integer.parseInt(scanner.nextLine());
                return choice;
            } catch (NumberFormatException e) {
                System.out.println("Невірний ввід. Введіть число.");
            }
        }
    }

    private Human createHuman(Scanner scanner) {
        System.out.println("Введіть ім'я: ");
        String name = scanner.nextLine();

        System.out.println("Введіть прізвище: ");
        String surname = scanner.nextLine();

        System.out.println("Введіть дату народження (рік, місяць, день): ");
        int birthYear = Integer.parseInt(scanner.nextLine());
        int birthMonth = Integer.parseInt(scanner.nextLine());
        int birthDay = Integer.parseInt(scanner.nextLine());

        System.out.println("Введіть IQ: ");
        int iq = Integer.parseInt(scanner.nextLine());

        return new Human(name, surname, birthYear, birthMonth, birthDay, iq);
    }

    private void executeChoice(int choice) {


        switch (choice) {
            case 1:
                Human mother = new Human("MotherName",
                        "MotherLastName", 1980, 1, 1, 100);
                Human father = new Human("FatherName",
                        "FatherLastName", 1975, 2, 2, 110);
                familyController.createNewFamily(mother, father);

                Human mother2 = new Human("MotherName2",
                        "MotherLastName2", 1978, 3, 5, 105);
                Human father2 = new Human("FatherName2",
                        "FatherLastName2", 1976, 1, 7, 115);
                familyController.createNewFamily(mother2, father2);

                System.out.println("Тестові дані створені.");
                break;
            case 2:
                List<Family> allFamilies = familyController.getAllFamilies();
                if (allFamilies.isEmpty()) {
                    System.out.println("Список сімей порожній.");
                }else {
                    System.out.println("Список усіх сімей: ");
                    for (int i =0; i < allFamilies.size(); i++) {
                        System.out.println("Сім'я " + (i + 1));
                        familyController.displayFamilyInfo(allFamilies.get(i));
                    }
                }
                //familyController.getAllFamilies();
                break;
            case 3:
                System.out.println("Введіть кількість членів для фільтрації більше за: ");
                int numberOfPeopleMax = getUserChoice();
                List<Family> familiesBigger = familyController.getFamiliesBiggerThan(numberOfPeopleMax);
                System.out.println("Сім'ї з кількістю людей більше "+numberOfPeopleMax+ ":");
                for (int i = 0; i < familiesBigger.size(); i++) {
                    System.out.println("Сім'я " + (i + 1));
                    familyController.displayFamilyInfo(familiesBigger.get(i));
                }
                System.out.println("Знайдено "+familiesBigger.size()+ " сімей.");
                break;
            case 4:
                System.out.println("Введіть кількість членів для фільтрації менше за::");
                int numberOfPeopleMin = getUserChoice();
                List<Family> familiesLess = familyController.getFamiliesLessThan(numberOfPeopleMin);
                System.out.println("Сім'ї з кількістю людей менше " + numberOfPeopleMin + ":");
                for (int i = 0; i < familiesLess.size(); i++) {
                    System.out.println("Сім'я " + (i + 1));
                    familyController.displayFamilyInfo(familiesLess.get(i));
                }
                System.out.println("Знайдено "+familiesLess.size()+ " сімей.");
                break;
            case 5:
                System.out.println("Введіть кількість членів для підрахунку:");
                int numberOfMembers = getUserChoice();
                long count = familyController.countFamiliesWithMemberNumber(numberOfMembers);
                System.out.println("Кількість сімей з " + numberOfMembers + " членами: " + count);
                break;
            case 6:
                System.out.println("Додавання нової сім'ї:");
                Human mother1 = createHuman(scanner);
                Human father1 = createHuman(scanner);
                familyController.createNewFamily(mother1, father1);
                //families.add(newFamily);
                System.out.println("Нова сім'я створена.");
                break;
            case 7:
                System.out.println("Введіть індекс сім'ї для видалення:");
                int familyIndex = getUserChoice();
                boolean deleted = familyController.deleteFamilyByIndex(familyIndex);
                if (deleted) {
                    System.out.println("Сім'ю за індексом " + familyIndex + " видалено.");
                } else {
                    System.out.println("Сім'ю за індексом " + familyIndex + " не знайдено.");
                }
                break;
            case 8:
                System.out.println("Введіть індекс сім'ї для редагування: ");
                int indexFamily = getUserChoice();
                if (indexFamily >= 0 && indexFamily < familyController.count()) {
                    System.out.println("Оберіть опцію для редагування:");
                    System.out.println("1. Народити дитину");
                    System.out.println("2. Усиновити дитину");
                    System.out.println("3. Повернутися до головного меню");
                    int editChoice = getUserChoice();

                    switch (editChoice) {
                        case 1:
                            System.out.println("Введіть ім'я хлопчика: ");
                            String boyName = scanner.nextLine();

                            System.out.println("Введіть ім'я дівчинки: ");
                            String girlName = scanner.nextLine();

                            Family family = familyController.getFamilyById(indexFamily);
                            try {
                                familyController.bornChild(family, boyName, girlName);
                                System.out.println("Дитина народилася.");
                            }catch (FamilyOverflowException e) {
                                System.err.println("Помилка: "+e.getMessage());
                            }
                            break;
                        case 2:
                            System.out.println("Введіть ПІБ дитини: ");
                            String childName = scanner.nextLine();
                            System.out.println("Введіть рік народження дитини: ");
                            String childBirthYear = Long.toString(getUserChoice());
                            System.out.println("Введіть рівень інтелекту дитини: ");
                            int childIq = getUserChoice();

                            Human child = new Human(childName, childBirthYear, childIq);
                            Family familyNew = familyController.getFamilyById(indexFamily);
                            try {
                                familyController.adoptChild(familyNew, child);
                                System.out.println("Дитина усиновлена.");
                            } catch (FamilyOverflowException e) {
                                System.err.println("Помилка: "+e.getMessage());
                            }
                            break;
                        case 3:
                            System.out.println("Повернення до головного меню.");
                            break;
                        default:
                            System.out.println("Невірний вибір.");
                            break;
                    }
                } else {
                    System.out.println("Сім'ю за вказаним індексом не знайдено.");
                }
                break;
            case 9:
                System.out.println("Введіть вік дітей для видалення: ");
                int age = getUserChoice();
                familyController.deleteAllChildrenOlderThan(age);
                System.out.println("Дітей старше " + age + " років видалено.");
                break;
            case 10:
                System.out.println("До побачення!");
                System.exit(0);
                break;
        }
    }
}
