import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        //-------------------//вызов методов из FamilyController//-------------------------//
        FamilyDao familyDao = new CollectionFamilyDao();
        FamilyService familyService = new FamilyService(familyDao);
        FamilyController familyController = new FamilyController(familyService);

        //cоздаем семью
        Human motherNew = new Human("Катерина", "Комарова", 1987);
        Human fatherNew = new Human("Дмитрий", "Комаров", 1983);
        Pet pet = new Dog("Rock", 5, 75,
                new String[]{"їсти", "пити", "спати"});
        familyController.createNewFamily(motherNew, fatherNew);
        familyController.addPet(0, pet);

        //информациюя о семьях
        familyController.displayAllFamilies();

        //delete family by index
        familyController.deleteFamilyByIndex(0);

        //информация о семьях после удаления
        familyController.displayAllFamilies();

        //рождения ребёнка
        Family familyNews = familyController.getFamilyById(0);
        Family newFamily = familyController.bornChild(familyNews, "Богдан", "Катерина");

        //информация о семьях c добавлениeм ребенка
        familyController.displayAllFamilies();

        //усыновление ребёнка
        Human newChild = new Human("Джеймс", "Комаров", 2012);
        familyController.adoptChild(newFamily, newChild);

        //информация о семьях после усыновления
        familyController.getAllFamilies();

        //удаляем детей
        familyController.deleteAllChildrenOlderThan(21);

        //информация о семьях после удаления детей
        familyController.displayAllFamilies();

        //количество семей
        int familyCount = familyController.count();
        System.out.println("Количество семей: " + familyCount);

        //количество питомцев
        List<Pet> pets = familyController.getPets(0);
        System.out.println("Kоличество питомцев в семьe: " + pets);

        //add питомцa в семью
        Pet newPet = new Dog("Buddy", 3, 30,
                new String[]{"есть", "гулять"});
        familyController.addPet(0, newPet);

        //список питомцев в семье
        pets = familyController.getPets(0);
        System.out.println("Cписок питомцев в семье после добавления нового: " + pets);


        //семья 1
        Human mother = new Human("Victoria", "Karleone", 1965);
        Human father = new Human("Vito", "Karleone", 1970);
         Pet pet1 = new Dog("Rock", 5, 75,
            new String[]{"їсти", "пити", "спати"});

        Map<String, String> schedule1 = new HashMap<>();
        schedule1.put(DaysWeek.MONDAY.name(), "робота");
        schedule1.put(DaysWeek.TUESDAY.name(), "відпочинок");

        Human child = new Human("Michael", "Karleone Jr., pet=Dog{nickname=Rock",
                1995, 90, pet1, mother, father, schedule1);

        Family family = new Family(mother, father);
        family.addChild(child);
        family.addPet(pet1);

        System.out.println("Інформація про дитину:");
        System.out.println(child.toString());

        System.out.println("Інформація про сім'ю:");
        System.out.println(family.toString());

        //семья 2
        Human mother1 = new Human("Мария", "Иванова", 1980);
        Human father1 = new Human("Иван", "Иванов", 1982);
        Pet fish = new Fish("Nemo", 2, 50,
                new String[]{"їсти", "плавати", "дихати водою"});

        Map<String, String> schedule2 = new HashMap<>();
        schedule2.put(DaysWeek.MONDAY.name(), "робота");
        schedule2.put(DaysWeek.TUESDAY.name(), "відпочинок");

        Human child1 = new Human("Анна", "Иванова",
                2005, 85, fish, mother1, father1, schedule2);

        Human child2 = new Human("Петр", "Иванов",
                2010, 75, fish, mother1, father1, schedule2);

        Family family1 = new Family(mother1, father1);
        family1.addChild(child1);
        family1.addChild(child2);
        family1.addPet(fish);


        //семья 3
        Human mother2 = new Human("Ольга", "Петрова", 1990);
        Human father2 = new Human("Алексей", "Петров", 1988);
        Pet homCat = new DomesticCat("Whiskers", 3, 60,
                new String[]{"їсти", "грати з м'ячем"});

        Map<String, String> schedule3 = new HashMap<>();
        schedule3.put(DaysWeek.MONDAY.name(), "робота");
        schedule3.put(DaysWeek.TUESDAY.name(), "відпочинок");

        Human child3 = new Human("Ирина", "Петрова",
                2015, 95, homCat, mother2, father2, schedule3);

        Family family2 = new Family(mother2, father2);
        family2.addChild(child3);
        family2.addPet(homCat);

        //выводим информацию о семьях
        System.out.println("Информация о семье 1:");
        System.out.println(family1.toString());

        System.out.println("Інформація про дитину:");
        System.out.println(child1.toString());

        System.out.println("Інформація про дитину:");
        System.out.println(child2.toString());


        System.out.println("\nИнформация о семье 2:");
        System.out.println(family2.toString());

        System.out.println("Інформація про дитину:");
        System.out.println(child3.toString());


        // Создаем большое количество объектов Human
        for (int i = 0; i < 100000; i++) {
            Human human = createHuman();
        }
    }

    private static Human createHuman() {
        return new Human("Имя", "Фамилия", 1990);
    }
}
