public class Girl extends Human {
    public Girl(String girlName, Human father, Human mother) {
        super(girlName, father.getSurname(), father.getYear());
    }
}