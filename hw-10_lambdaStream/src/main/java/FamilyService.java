import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class FamilyService {
    private final FamilyDao familyDao;

    public FamilyService(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public void displayAllFamilies() {
        familyDao.getAllFamilies()
                .forEach(family -> {
                    System.out.println("Family: ");
                    family.prettyFormat();
                });
    }

    public List<Family> getFamiliesBiggerThan(int numberOfPeople) {
        return familyDao.getAllFamilies()
                .stream()
                .filter(family -> family.countFamily() > numberOfPeople)
                .collect(Collectors.toList());
    }

    public List<Family> getFamiliesLessThan(int numberOfPeople) {
        return familyDao.getAllFamilies()
                .stream()
                .filter(family -> family.countFamily() < numberOfPeople)
                .collect(Collectors.toList());
    }

    public long countFamiliesWithMemberNumber(int numberOfPeople) {
        return familyDao.getAllFamilies()
                .stream()
                .filter(family -> family.countFamily() == numberOfPeople)
                .count();
    }

    public void createNewFamily(Human mother, Human father) {
        Family newFamily = new Family(mother, father);
        familyDao.saveFamily(newFamily);

    }

    public boolean deleteFamilyByIndex(int index) {
        if (familyDao.deleteFamily(index)) {
            return true;
        }
        return false;
    }

    public Family bornChild(Family family, String boyName, String girlName) {
        if (family != null) {
            Human child;
            if (Math.random() < 0.5) {
                child = new Boy(boyName, family.getFather(), family.getMother());
            } else {
                child = new Girl(girlName, family.getFather(), family.getMother());
            }
            family.addChild(child);
            familyDao.saveFamily(family);
            return family;
        }
        return null;
    }

    public Family adoptChild(Family family, Human child) {
        if (family != null) {
            family.addChild(child);
            familyDao.saveFamily(family);
            return family;
        }
        return null;
    }

    public void deleteAllChildrenOlderThan(int age) {
        familyDao.getAllFamilies()
                .forEach(family -> {
                    family.deleteAllChildrenOlderThan(age);
                    familyDao.saveFamily(family);
                });
    }

    public int count() {
        return familyDao.getAllFamilies().size();
    }

    public Family getFamilyById(int index) {
        List<Family> families = familyDao.getAllFamilies();
        if (index >= 0 && index < families.size()) {
            return families.get(index);
        }
        return null;
    }

    public List<Pet> getPets(int familyIndex) {
        List<Family> families = familyDao.getAllFamilies();
        if (familyIndex >= 0 && familyIndex < families.size()) {
            Set<Pet> petSet = families.get(familyIndex).getPets();
            //System.out.println(families.get(familyIndex));
            return new ArrayList<>(petSet);
        }
        return Collections.emptyList();
    }

    public void addPet(int familyIndex, Pet pet) {
        List<Family> families = familyDao.getAllFamilies();
        if (familyIndex >= 0 && familyIndex < families.size()) {
            Family family = families.get(familyIndex);
            family.addPet(pet);
            familyDao.saveFamily(family);
        }
    }

}
