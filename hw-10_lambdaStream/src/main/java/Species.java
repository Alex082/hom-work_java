public enum Species {
    FISH,
    DOMESTIC_CAT,
    DOG,
    ROBOCAT,
    UNKNOWN,
}
