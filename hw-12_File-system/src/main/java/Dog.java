public class Dog extends Pet implements Fouling {
    public Dog(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, Species.DOG);
        setAge(age);
        setTrickLevel(trickLevel);
        for (String habit : habits) {
            addHabit(habit);
        }
    }

    @Override
    public void respond() {
        System.out.println("Привіт, я собака. Мене звуть " + getNickname());
    }

    @Override
    public void foul() {
        System.out.println("Собаки роблять гидоти на вулиці");
    }

    public static void main(String[] args) {
        Dog dog = new Dog("Rock", 5, 75, new String[]{"їсти", "пити", "спати"});

        System.out.println("Демонстрація методів eat, respond, foul:");
        dog.eat();
        dog.respond();
        //dog.foul();
        if (dog instanceof Fouling) {
            ((Fouling) dog).foul();
        }
    }
}
