import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class FamilyController {
    private final FamilyService familyService;

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;
    }

    public List<Family> getAllFamilies() {
        List<Family> allFamilies = familyService.getAllFamilies();
        if (allFamilies == null) {
            return new ArrayList<>();
        }
        return allFamilies;
        //return familyService.getAllFamilies();
    }

    public void displayAllFamilies() {
        familyService.displayAllFamilies();
    }

    public List<Family> getFamiliesBiggerThan(int numberOfPeople) {
        return familyService.getFamiliesBiggerThan(numberOfPeople);
    }

    public List<Family> getFamiliesLessThan(int numberOfPeople) {
        return familyService.getFamiliesLessThan(numberOfPeople);
    }

    public long countFamiliesWithMemberNumber(int numberOfMembers) {
        List<Family> allFamilies = familyService.getAllFamilies();
        if (allFamilies == null) {
            return 0;
        }

        return allFamilies.stream()
                .filter(family -> family.countFamily() == numberOfMembers)
                .count();
        //return familyService.countFamiliesWithMemberNumber(numberOfMembers);
    }

    public void createNewFamily(Human mother, Human father) {
        familyService.createNewFamily(mother, father);
    }

    public boolean deleteFamilyByIndex(int index) {
        return familyService.deleteFamilyByIndex(index);
    }

    public Family bornChild(Family family, String boyName, String girlName) {
        if (family != null) {
            Human child;
            if (Math.random() < 0.5) {
                child = new Boy(boyName, family.getFather(), family.getMother());
            } else {
                child = new Girl(girlName, family.getFather(), family.getMother());
            }
            family.addChild(child);
            return family;
        }
        return null;
    }

    public Family adoptChild(Family family, Human child) {
        if (family != null) {
            family.addChild(child);
            return family;
        }
        return null;
    }

    public void deleteAllChildrenOlderThan(int age) {
        familyService.deleteAllChildrenOlderThan(age);
    }

    public int count() {
        return familyService.count();
    }

    public Family getFamilyById(int index) {
        return familyService.getFamilyById(index);
    }

    public List<Pet> getPets(int index) {
        return familyService.getPets(index);
    }

    public void addPet(int index, Pet pet) {
        familyService.addPet(index, pet);
    }

    public void displayFamilyInfo(Family family) {
        System.out.println("family:");
        System.out.println(" mother: " + family.getMother());
        System.out.println(" father: " + family.getFather());

        List<Human> children = family.getChildren();
        if (!children.isEmpty()) {
            System.out.println(" children:");
            for (Human child : children) {
                System.out.println("  " + child.getGender() + ": " + child);
            }
        }

        Set<Pet> petSet = family.getPets();
        List<Pet> petList = new ArrayList<>(petSet);
        System.out.println("   pets: " + petList);
    }

    public void saveData() {
        familyService.saveData();
    }

    public void loadData(){
        familyService.loadData();
    }
}
