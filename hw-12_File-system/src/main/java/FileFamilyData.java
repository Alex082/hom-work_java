import java.io.*;
import java.util.List;

public class FileFamilyData {
    private String fileName;

    public FileFamilyData(String fileName) {
        this.fileName = fileName;
    }

    public void saveData(List<Family> families) {
        try(ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream(fileName))) {
            output.writeObject(families);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Family> loadData() {
        List<Family> families = null;
        try(ObjectInputStream input = new ObjectInputStream(new FileInputStream(fileName))) {
            families = (List<Family>) input.readObject();
        }catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return families;
    }
}
