public class Boy extends Human {
    public Boy(String boyName, Human father, Human mother) {
        super(boyName, father.getSurname(), father.getYear());
        setGender(Gender.boy);
    }
}
