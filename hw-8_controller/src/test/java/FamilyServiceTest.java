import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;
public class FamilyServiceTest {
    private FamilyService familyService;

    @Before
    public void setUp(){
        FamilyDao familyDao = new CollectionFamilyDao();
        familyService = new FamilyService(familyDao);
    }

    @Test
    public void testGetAllFamilies() {
        List<Family> families = familyService.getAllFamilies();
        assertNotNull(families);
        assertEquals(0, families.size());
    }

    @Test
    public void testCreateNewFamily() {
        Human mother = new Human("Mother", "LastName", 1980);
        Human father = new Human("Father", "LastName", 1985);
        familyService.createNewFamily(mother, father);

        List<Family> families = familyService.getAllFamilies();
        assertNotNull(families);
        assertEquals(1, families.size());
    }

    @Test
    public void testDeleteFamilyByIndex() {
        Human mother = new Human("Mother", "LastName", 1980);
        Human father = new Human("Father", "LastName", 1985);
        familyService.createNewFamily(mother, father);

        boolean deleted = familyService.deleteFamilyByIndex(0);
        assertTrue(deleted);

        List<Family> families = familyService.getAllFamilies();
        assertNotNull(families);
        assertEquals(0, families.size());
    }

    @Test
    public void testBornChild() {
        Human mother = new Human("Mother", "LastName", 1980);
        Human father = new Human("Father", "LastName", 1985);
        familyService.createNewFamily(mother, father);

        Family family = familyService.getFamilyById(0);
        assertNotNull(family);

        Family newFamily = familyService.bornChild(family, "BoyName", "GirlName");
        assertNotNull(newFamily);

        List<Human> children = newFamily.getChildren();
        assertNotNull(children);
        assertEquals(1, children.size());
    }

    @Test
    public void testGetFamiliesBiggerThan() {
        Human mother1 = new Human("Mother1", "LastName1", 1980);
        Human father1 = new Human("Father1", "LastName1", 1985);
        Human mother2 = new Human("Mother2", "LastName2", 1982);
        Human father2 = new Human("Father2", "LastName2", 1987);

        familyService.createNewFamily(mother1, father1);
        familyService.createNewFamily(mother2, father2);

        List<Family> families = familyService.getFamiliesBiggerThan(2);
        assertNotNull(families);
        assertEquals(0, families.size());
    }

    @Test
    public void testGetFamiliesLessThan() {
        Human mother1 = new Human("Mother1", "LastName1", 1980);
        Human father1 = new Human("Father1", "LastName1", 1985);
        Human mother2 = new Human("Mother2", "LastName2", 1982);
        Human father2 = new Human("Father2", "LastName2", 1987);

        familyService.createNewFamily(mother1, father1);
        familyService.createNewFamily(mother2, father2);

        List<Family> families = familyService.getFamiliesLessThan(3);
        assertNotNull(families);
        assertEquals(2, families.size());
    }

    @Test
    public void testCountFamiliesWithMemberNumber() {
        Human mother1 = new Human("Mother1", "LastName1", 1980);
        Human father1 = new Human("Father1", "LastName1", 1985);
        Human mother2 = new Human("Mother2", "LastName2", 1982);
        Human father2 = new Human("Father2", "LastName2", 1987);

        familyService.createNewFamily(mother1, father1);
        familyService.createNewFamily(mother2, father2);

        int count = familyService.countFamiliesWithMemberNumber(4);
        assertEquals(0, count);
    }

    @Test
    public void testDeleteAllChildrenOlderThan() {
        Human mother1 = new Human("Mother1", "LastName1", 1980);
        Human father1 = new Human("Father1", "LastName1", 1985);
        familyService.createNewFamily(mother1, father1);

        Family family = familyService.getFamilyById(0);
        Human child1 = new Human("Child1", "LastName1", 2000);
        family.addChild(child1);

        familyService.deleteAllChildrenOlderThan(21);

        List<Human> children = family.getChildren();
        assertEquals(1, children.size());
    }

    @Test
    public void testAdoptChild() {
        Human mother = new Human("Mother", "LastName", 1980);
        Human father = new Human("Father", "LastName", 1985);
        familyService.createNewFamily(mother, father);

        Family family = familyService.getFamilyById(0);
        assertNotNull(family);

        Human child = new Human("AdoptedChild", "LastName", 2000);
        Family newFamily = familyService.adoptChild(family, child);
        assertNotNull(newFamily);

        List<Human> children = newFamily.getChildren();
        assertNotNull(children);
        assertEquals(1, children.size());
    }

    @Test
    public void testCount() {
        Human mother1 = new Human("Mother1", "LastName1", 1980);
        Human father1 = new Human("Father1", "LastName1", 1985);
        Human mother2 = new Human("Mother2", "LastName2", 1982);
        Human father2 = new Human("Father2", "LastName2", 1987);

        familyService.createNewFamily(mother1, father1);
        familyService.createNewFamily(mother2, father2);

        int count = familyService.count();
        assertEquals(2, count);
    }

    @Test
    public void testGetPets() {
        Human mother = new Human("Mother", "LastName", 1980);
        Human father = new Human("Father", "LastName", 1985);
        familyService.createNewFamily(mother, father);

        Pet pet = new Dog("PetName", 2, 10, new String[]{"eat", "sleep"});
        familyService.addPet(0, pet);

        List<Pet> pets = familyService.getPets(0);
        assertNotNull(pets);
        assertEquals(1, pets.size());
    }

    @Test
    public void testAddPet() {
        Human mother = new Human("Mother", "LastName", 1980);
        Human father = new Human("Father", "LastName", 1985);
        familyService.createNewFamily(mother, father);

        Pet pet = new DomesticCat("PetName", 3, 7, new String[]{"Clean up", "Charge"});
        familyService.addPet(0, pet);

        List<Pet> pets = familyService.getPets(0);
        assertNotNull(pets);
        assertEquals(1, pets.size());
    }
}
