public final class Man extends Human{
    public Man(String name, String surname, int year){
        super(name, surname, year);
    }
    @Override
    public void greetPet() {
        System.out.println("Привіт, мій друг!");
    }

    public void repairCar(){
        System.out.println("Лагоджу авто");
    }

    public static void main(String[] args) {
        Man man = new Man("John", "Doe", 1985);

        man.greetPet();
        man.repairCar();
    }
}
