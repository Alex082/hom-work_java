public final class Woman extends Human{
    public Woman(String name, String surname, int year){
        super(name, surname, year);
    }

    @Override
    public void greetPet(){
        System.out.println("Привіт, мій мурчачий друг!");
    }

    public void makeup() {
        System.out.println("Наношу макіяж");
    }

    public static void main(String[] args) {
        Woman woman = new Woman("Alice", "Smith", 1990);

        woman.greetPet();
        woman.makeup();
    }
}
