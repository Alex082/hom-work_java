import java.util.*;

public class Family {
    private Human mother;
    private Human father;
    private List<Human> children;
    private Set<Pet> pets;
    private String species;

    private Pet pet;
    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        this.children = new ArrayList<>();
        this.pets = new HashSet<>();
        mother.setFamily(this);
        father.setFamily(this);
    }

    public void addChild(Human child) {
        children.add(child);
        child.setFamily(this);
    }

    public List<Human> getChildren() {
        return children;
    }

    public void addPet(Pet pet) {
        pets.add(pet);
    }

    public Set<Pet> getPets() {
        return pets;
    }

    public boolean removeChild(Human child) {
        return children.remove(child);
    }


    public int countFamily() {
        return 2 + children.size();
    }

    public Human getMother() {
        return mother;
    }

    public Human getFather() {
        return father;
    }

    public boolean removePet(Pet pet) {
        return pets.remove(pet);
    }


    @Override
    public String toString() {
        List<String> childNames = new ArrayList<>();
        for (Human child : children) {
            childNames.add(child.getName() + " " + child.getSurname());
        }
        String childrenList = String.join(", ", childNames);

        List<String> petInfoList = new ArrayList<>();
        for (Pet pet : pets) {
            petInfoList.add(pet.getSpecies().name() + "{" + "nickname='" + pet.getNickname() + "'}");
        }
        String petList = String.join(", ", petInfoList);

        if (petList.isEmpty()) {
            petList = "немає домашніх улюбленців";
        }

        return "Family{mother=" + mother.getName() + " " + mother.getSurname() + "," +
                " father=" + father.getName() + " " + father.getSurname() + "," +
                " children=[" + childrenList + "], pets=[" + petList + "]}";
    }

    public int indexof(Human child) {
        return children.indexOf(child);
    }

    public void prettyFormat() {
    }

    public void deleteAllChildrenOlderThan(int age) {
    }
}
