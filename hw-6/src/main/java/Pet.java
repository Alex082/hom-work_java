import java.util.Arrays;
import java.util.Objects;

public abstract class Pet {
    private Species species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    public Pet(String nickname, Species species) {
        this.species = species;
        this.nickname = nickname;
    }

    public Pet(Species species, String nickname, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public Pet() {
    }

    public static void main(String[] args) {
        Pet pet = new Dog("Rock", 5, 75,
                new String[]{"їсти", "пити", "спати"});

        System.out.println("Демонстрація методів eat, respond, foul:");
        pet.eat();
        pet.respond();
        //pet.foul();
        if (pet instanceof Fouling) {
            ((Fouling) pet).foul();
        }else {
            System.out.println("Ця тварина не робить гидоти");
        }
    }


    public void eat() {
        System.out.println("Я їм!");
    }

    public abstract void respond();
    //public abstract void respond() {
    //    System.out.println("Привіт, хазяїн. Я - " + nickname + ". Я скучив!");
    //}

    //public abstract void foul();
    public void foul() {
        System.out.println("Потрібно добре замести сліди...");
    }

    public Species getSpecies() {
        return species;
    }

    public String getNickname() {
        return nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return age == pet.age &&
                trickLevel == pet.trickLevel &&
                species == pet.species &&
                Objects.equals(nickname, pet.nickname) &&
                Arrays.equals(habits, pet.habits);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(species, nickname, age, trickLevel);
        result = 31 * result + Arrays.hashCode(habits);
        return result;
    }


    @Override
    public String toString() {
        return species + "{nickname='" + nickname + "'," +
                " age=" + age + "," +
                " trickLevel=" + trickLevel + "," +
                " habits=" + Arrays.toString(habits) + "}";
    }
}
