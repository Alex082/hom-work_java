import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;

public class Family {
    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;
    private String species;

    public Family(Human mother, Human father, Pet pet) {
        this.mother = mother;
        this.father = father;
        this.children = new Human[0];
        this.pet = pet;
        this.species = pet.getSpecies().name();
        mother.setFamily(this);
        father.setFamily(this);
    }

    public void addChild(Human child) {
        int length = children.length;
        Human[] newChildren = new Human[length + 1];
        System.arraycopy(children, 0, newChildren, 0, length);
        newChildren[length] = child;
        children = newChildren;
        child.setFamily(this);
    }

    public boolean deleteChild(int index) {
        if (children == null || index < 0 || index >= children.length) {
            return false;
        }

        Human[] newChildren = new Human[children.length - 1];
        for (int i = 0, a = 0; i < children.length; i++) {
            if (i != index) {
                newChildren[a++] = children[i];
            }
        }
        children = newChildren;
        return true;

    }

    public int countFamily() {
        return 2 + children.length;
    }

    public Human getMother() {
        return mother;
    }

    public Human getFather() {
        return father;
    }

    public Human[] getChildren() {
        return children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    @Override
    public String toString() {
    //    StringBuilder childrenNames = new StringBuilder();
    //    for (Human child : children) {
    //        childrenNames.append(child.getName() + " " +child.getSurname());
    //    }
    //    String childrenList = childrenNames.toString().trim().replaceAll(", $", "");

    StringJoiner childrenNames = new StringJoiner(", ");
        for (Human child : children) {
        childrenNames.add(child.getName() + " " + child.getSurname());
    }
    String childrenList = childrenNames.toString();

        String petInfo = (pet != null) ? species + "{" + "nickname='" + pet.getNickname() + "'}"
                : "немає домашнього улюбленця";

        return "Family{mother=" + mother.getName() + " " + mother.getSurname() + "," +
                " father=" + father.getName() + " " + father.getSurname() + "," +
                " children=[" + childrenList + "], pet=" + petInfo + "}";
    }

    public int indexof(Human child) {
        List<Human> childrenList = Arrays.asList(children);
        return childrenList.indexOf(child);
    }
}
