public class DomesticCat extends Pet implements Fouling {
    public DomesticCat(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, Species.DOMESTIC_CAT);
        setAge(age);
        setTrickLevel(trickLevel);
        setHabits(habits);
    }

    @Override
    public void respond() {
        System.out.println("Привіт, я домашня кішка. Мене звуть "+getNickname());
    }

    @Override
    public void foul() {
        System.out.println("Домашні кішки роблять гидоти в лотку");
    }

    public static void main(String[] args) {
        Pet domesticCat = new DomesticCat("Whiskers", 3, 60,
                new String[]{"їсти", "грати з м'ячем"});

        System.out.println("Демонстрація методів eat, respond, foul:");
        domesticCat.eat();
        domesticCat.respond();
        if (domesticCat instanceof Fouling) {
            ((Fouling) domesticCat).foul();
        }
        //pet3.foul();
    }
}
