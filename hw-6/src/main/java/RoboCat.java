public class RoboCat extends Pet {
    public RoboCat(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, Species.ROBOCAT);
        setAge(age);
        setTrickLevel(trickLevel);
        setHabits(habits);
    }
    @Override
    public void respond() {
        System.out.println("Привіт, я кіт-робот. Мене звуть "+getNickname());
    }

    public static void main(String[] args) {
        Pet roboCat = new RoboCat("R2D2", 1, 70,
                new String[]{"робити біп-біп", "танцювати"});

        roboCat.eat();
        roboCat.respond();
        if (roboCat instanceof Fouling) {
            ((Fouling) roboCat).foul();
        }
    }
}
