public class Fish extends Pet {
    public Fish(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname,Species.FISH);
        setAge(age);
        setTrickLevel(trickLevel);
        setHabits(habits);
    }

    @Override
    public void respond(){
        System.out.println("Привіт, я рибка. Мене звуть "+getNickname());
    }

    public static void main(String[] args) {
        Pet fish = new Fish("Nemo", 2, 50,
                new String[]{"їсти", "плавати", "дихати водою"});

           System.out.println("Демонстрація методів eat, respond, foul:");
            fish.eat();
            fish.respond();
        if (fish instanceof Fouling) {
            ((Fouling) fish).foul();
        }
        }
}
