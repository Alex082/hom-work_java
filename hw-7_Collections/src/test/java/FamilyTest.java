import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class FamilyTest {
    private Family family;
    private Human mother;
    private Human father;
    private Pet pet;

    @Before
    public void setUp() {
        mother = new Human("Victoria", "Karleone", 1965);
        father = new Human("Vito", "Karleone", 1970);

        Set<String> habits = new HashSet<>(Arrays.asList("їсти", "пити", "спати"));
        String[] habitsArray = habits.toArray(new String[0]);

        pet = new Dog("Rock", 5, 75, habitsArray);


        Map<String, String> schedule = new HashMap<>();
        schedule.put(DaysWeek.MONDAY.name(), "робота");
        schedule.put(DaysWeek.TUESDAY.name(), "відпочинок");

        family = new Family(mother, father);

        Human child = new Human("Michael", "Karleone Jr.",
                1995, 90, pet, mother, father, schedule);

        family.addChild(child);
        family.addPet(pet);
    }

    @Test
    public void testToString() {
        String expected = "Family{mother=Victoria Karleone, father=Vito Karleone," +
                " children=[Michael Karleone Jr.], pets=[DOG{nickname='Rock'}]}";
        String actual = family.toString();

        //System.out.println("Expected: " + expected);
        //System.out.println("Actual: " + actual);

        assertEquals(expected, actual);
    }

    @Test
    public void testDeleteChild() {
        Human child = family.getChildren().get(0);
        assertTrue(family.removeChild(child));
    }

    @Test
    public void testDeleteChildByIndex() {
        if (family.getChildren().size() > 0) {
            Human childRemove = family.getChildren().get(0);
            assertTrue(family.removeChild(childRemove));
        }
    }

    @Test
    public void testAddChild() {
        Human newChild = new Human("Test", "Child", 2000);
        family.addChild(newChild);
        assertEquals(2, family.getChildren().size());
    }

    @Test
    public void testAddChildNegative() {
        assertFalse(family.removeChild(null));
        assertFalse(family.removeChild(new Human("Nonexistent", "Child", 2000)));
    }

    @Test
    public void testCountFamily() {
        assertEquals(3, family.countFamily());
    }
}
