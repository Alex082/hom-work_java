import java.util.Objects;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Map<String, String> schedule;
    private Set<String> habits;
    private Family family;

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.schedule = new HashMap<>();
        this.habits = new HashSet<>();
    }

    public Human(String name, String surname, int year, Human mother, Human father) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name,
                 String surname,
                 int year, int iq,
                 Pet pet, Human mother,
                 Human father, Map<String, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;

        this.family = new Family(mother, father);
        mother.setFamily(this.family);
        father.setFamily(this.family);
    }

    public static void main(String[] args) {
        Pet pet = new Dog("Rock", 5, 75,
                new String[]{"їсти", "пити", "спати"});

        Human mother = new Human("Jane", "Karleone", 1970);
        Human father = new Human("Vito", "Karleone", 1965);

        Map<String, String> schedule = new HashMap<>();
        schedule.put("Понеділок", "робота");
        schedule.put("Вівторок", "відпочинок");

        Family family = new Family(mother, father);
        family.addPet(pet);

        Human child = new Human("Michael", "Karleone Jr.",
                1995, 90, pet, mother, father, schedule);
        family.addChild(child);

        System.out.println("Демонстрація методів greetPet та describePet:");
        child.greetPet();
        child.describePet();
    }

    public Map<String, String> getSchedule() {
        return schedule;
    }

    public void addToSchedule(String day, String activity) {
        schedule.put(day, activity);
    }

    public Set<String> getHabits() {
        return habits;
    }

    public void addHabit(String habit) {
        habits.add(habit);
    }

    public void greetPet() {
        for (Pet pet : family.getPets()) {
            System.out.println("Привіт, " + pet.getNickname() + "!");
        }
    }

    public void describePet() {
        for (Pet pet : family.getPets()) {
            String intelligence = (pet.getTrickLevel() > 50) ? "дуже хитрий" : "не дуже хитрий.";
            System.out.println("В мене є " + pet.getSpecies() +
                    ", їй " + pet.getAge() + " років, він " + intelligence);
        }
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getYear() {
        return year;
    }

    public int getIq() {
        return iq;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return year == human.year &&
                iq == human.iq &&
                Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, year, iq);
    }


    @Override
    public String toString() {
        String motherName = (family.getMother() != null) ?
                family.getMother().getName() + " " + family.getMother().getSurname()
                : "немає інформації про матір";
        String fatherName = (family.getFather() != null) ?
                family.getFather().getName() + " " + family.getFather().getSurname()
                : "немає інформації про батька";
        String petInfo = (family.getPets() != null) ? family.getPets().toString()
                : "немає домашнього улюбленця";

        return "Human{name='" + name + "'," +
                " surname='" + surname + "'," +
                " year=" + year + ", iq=" + iq + "," +
                " mother=" + motherName + "," +
                " father=" + fatherName + "," +
                " pet=" + petInfo + "}";
    }
}
