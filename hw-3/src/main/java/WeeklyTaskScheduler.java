import java.util.Scanner;

public class WeeklyTaskScheduler {
    public static void main(String[] args) {
        String[][] scedule = new String[7][2];
        scedule[0][0] = "Sunday";
        scedule[0][1] = "do home work";
        scedule[1][0] = "Monday";
        scedule[1][1] = "go to courses; watch a film";
        scedule[2][0] = "Tuesday";
        scedule[2][1] = "meeting with friends";
        scedule[3][0] = "Wednesday";
        scedule[3][1] = "go to the gym";
        scedule[4][0] = "Thursday";
        scedule[4][1] = "work on the project";
        scedule[5][0] = "Friday";
        scedule[5][1] = "go to a concert";
        scedule[6][0] = "Saturday";
        scedule[6][1] = "relax and read a book";

        Scanner scanner = new Scanner(System.in);
        String input;

        while (true) {
            System.out.print("Please, input the day of the week: ");
            input = scanner.nextLine().trim().toLowerCase();

            if (input.equals("exit")) {
                break;
            }

            String task = null;

            for (int i = 0; i < scedule.length; i++) {
                if (scedule[i][0].toLowerCase().equals(input)) {
                    task = scedule[i][1];
                    break;
                }
            }

            if (task != null) {
                System.out.println("Your tasks for" + scedule[1][0] + ": " + task);
            } else {
                System.out.println("Sorry, I don't understand you, please try again.");
            }
        }

        System.out.println("Goodbye!");
        scanner.close();
    }
}
