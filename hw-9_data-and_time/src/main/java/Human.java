import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class Human {
    private String name;
    private String surname;
    private long birthDate;
    private int iq;
    private Map<String, String> schedule;
    private Set<String> habits;
    private Family family;

    public Human(String name, String surname, long birthDate) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.schedule = new HashMap<>();
        this.habits = new HashSet<>();
    }

    public Human(String name, String surname, String birthDateStr, int iq){
        this.name = name;
        this.surname = surname;
        DateTimeFormatter formatter = DateTimeFormatter
                .ofPattern("dd/MM/yyyy");
        LocalDate localDate = LocalDate.parse(birthDateStr, formatter);
        this.birthDate = localDate
                .atStartOfDay(ZoneId.systemDefault())
                .toInstant().toEpochMilli();
        this.iq = iq;
        this.schedule = new HashMap<>();
        this.habits = new HashSet<>();
    }

    public Human(String name, String surname, long birthDate, Human mother, Human father) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
    }

    public Human(String name,
                 String surname,
                 long birthDate, int iq,
                 Pet pet, Human mother,
                 Human father, Map<String, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.iq = iq;
        this.schedule = schedule;

        this.family = new Family(mother, father);
        mother.setFamily(this.family);
        father.setFamily(this.family);
    }

    public static void main(String[] args) {
        Pet pet = new Dog("Rock", 5, 75,
                new String[]{"їсти", "пити", "спати"});

        Human mother = new Human("Jane", "Karleone", 1970);
        Human father = new Human("Vito", "Karleone", 1965);

        Map<String, String> schedule = new HashMap<>();
        schedule.put("Понеділок", "робота");
        schedule.put("Вівторок", "відпочинок");

        Family family = new Family(mother, father);
        family.addPet(pet);

        Human child = new Human("Michael", "Karleone Jr.",
                1995, 90, pet, mother, father, schedule);
        family.addChild(child);

        System.out.println("Демонстрація методів greetPet та describePet:");
        child.greetPet();
        child.describePet();
    }

    public String describeAge() {
        LocalDate birthLocalDate = Instant
                .ofEpochMilli(birthDate)
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
        LocalDate currentDate = LocalDate.now();

        Period period = Period.between(birthLocalDate, currentDate);
        return String.format("%d years, %d mounts, %d days",
                period.getYears(), period.getMonths(), period.getDays());
    }

    public Map<String, String> getSchedule() {
        return schedule;
    }

    public void addToSchedule(String day, String activity) {
        schedule.put(day, activity);
    }

    public Set<String> getHabits() {
        return habits;
    }

    public void addHabit(String habit) {
        habits.add(habit);
    }

    public void greetPet() {
        for (Pet pet : family.getPets()) {
            System.out.println("Привіт, " + pet.getNickname() + "!");
        }
    }

    public void describePet() {
        for (Pet pet : family.getPets()) {
            String intelligence = (pet.getTrickLevel() > 50) ? "дуже хитрий" : "не дуже хитрий.";
            System.out.println("В мене є " + pet.getSpecies() +
                    ", їй " + pet.getAge() + " років, він " + intelligence);
        }
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public long getYear() {
        return birthDate;
    }

    public int getIq() {
        return iq;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return birthDate == human.birthDate &&
                iq == human.iq &&
                Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, birthDate, iq);
    }

    @Override
    public String toString(){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate localDate = Instant.ofEpochMilli(birthDate)
                .atZone(ZoneId.systemDefault()).toLocalDate();
        return String.format("Human{name='%s', surname='%s', birthDate='%s', iq=%d}",
                name, surname, formatter.format(localDate), iq);

    }
}
